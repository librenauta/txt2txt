class CreateMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :messages do |t|
      t.text :body
      t.text :translated_body
      t.string :uuid
      t.string :author
      t.string :receiver

      t.timestamps
    end
  end
end
