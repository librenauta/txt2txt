Rails.application.routes.draw do
  root 'messages#index'
  resources :messages do
    member do
      get :decrypt
    end
  end
end
